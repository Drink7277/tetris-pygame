import sys, os
import pygame
from const import *
from menu import MenuItem


class GameMenu(object):
    WHITE =(255, 255, 255)
    ORANGE = (255,150,0)
    def __init__(self,menuops,background,resource,pos= None):
        self.resource = resource
        self.background = background
        if pos is None:
            self.set_textpos(menuops)
        else:
            self.set_textpos(menuops,pos[0],pos[1])
        self.cur_item = None

    # formating menu button,
    # including main menu and new score menu, pause game menu
    def set_textpos(self, menuops, posx=None, posy=None):
        self.items = []
        if not posx is None and not posy is None :
            for index, item in enumerate(menuops):
                menu_item = MenuItem(item)
                font_height = menu_item.get_height()
                pos_x = posx - (menu_item.width / 2)
                pos_y = posy  + (index * 2 * font_height)
                menu_item.set_position(pos_x, pos_y)

                self.items.append(menu_item)

        else:
            for index, item in enumerate(menuops):
                menu_item = MenuItem(item)
                font_height = menu_item.get_height()
                pos_x = (SCREEN_SIZE()[0] / 4 * 3) - (menu_item.width / 2)
                pos_y = (SCREEN_SIZE()[1] / 4)  + (index * 2 * font_height)
                menu_item.set_position(pos_x, pos_y)

                self.items.append(menu_item)

    def draw(self,surface):
        if not self.background is None:
            surface.blit(self.background,(0,0))
        for item in self.items:
            surface.blit(item.label, item.position)
    def subControl(self,event):
        if event.type == pygame.QUIT:
            exit()
        elif event.type == pygame.MOUSEBUTTONDOWN or\
             (event.type == pygame.KEYDOWN and \
             (event.key == pygame.K_SPACE or \
             event.key == pygame.K_RETURN)):
                if not self.cur_item is None:
                    pygame.mouse.set_visible(True)
                    return self.items[self.cur_item].text
        elif event.type == pygame.KEYDOWN:
            pygame.mouse.set_visible(False)
            self.set_keyboard_selection(event.key)
        elif pygame.mouse.get_rel() != (0, 0):
            pygame.mouse.set_visible(True)
            self.set_mouse_selection()

    def control(self):
        for event in pygame.event.get():
            return self.subControl(event)

    def selectItem(self,n):
        if self.cur_item == n :
            return

        self.cur_item = n

        self.resource.playSound('move')

        for i in xrange(len(self.items)):
            if i != n:
                self.items[i].set_italic(False)
                self.items[i].set_font_color(GameMenu.WHITE)
            else:
                self.items[i].set_italic(True)
                self.items[i].set_font_color(GameMenu.ORANGE)

    def set_keyboard_selection(self, key):
        if key != pygame.K_UP and key != pygame.K_DOWN  :
            return
        if self.cur_item is None:
            self.selectItem(0)
        else:
            # Find the chosen item
            if key == pygame.K_UP and self.cur_item > 0:
                self.selectItem(self.cur_item - 1)
            elif key == pygame.K_UP and self.cur_item == 0:
                self.selectItem(len(self.items) - 1)
            elif key == pygame.K_DOWN and self.cur_item < len(self.items) - 1:
                self.selectItem(self.cur_item + 1)
            elif key == pygame.K_DOWN and self.cur_item == len(self.items) - 1:
                self.selectItem(0)

    def set_mouse_selection(self):
        for i in xrange(len(self.items)):
            if self.mouse_select_menu(self.items[i]):
                self.selectItem(i)
                return
        self.selectItem(None)

    def mouse_select_menu(self, item):
        posx, posy = pygame.mouse.get_pos()
        if (posx >= item.pos_x and posx <= item.pos_x + item.width) and \
           (posy >= item.pos_y and posy <= item.pos_y + item.height):
            return True
        return False

# show the top users' score
class Score(object):
    def __init__(self,background,pos,resource):
        self.resource = resource
        self.pos    = pos
        self.background = background

    # show all users' score who are five highest
    def control(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.MOUSEBUTTONDOWN or\
                 (event.type == pygame.KEYDOWN and \
                 (event.key == pygame.K_SPACE or \
                  event.key == pygame.K_RETURN or \
                  event.key == pygame.K_ESCAPE)):
                        return 'Menu'
            elif event.type == pygame.KEYDOWN:
                pygame.mouse.set_visible(False)

    # draw score board
    def draw(self,surface):
        surface.blit(self.background,(0,0))

        fontSize = 28
        fontColor = (255,150,0)
        x ,y     = self.pos
        dy       = 30
        score    = self.resource.getHighScore()
        pos = (x,y)
        for i in xrange(len(score)):

            label = pygame.font.Font(None, fontSize).render(str(i+1)+'.'+ str(score[i][2]) , 1,fontColor)
            surface.blit(label,pos)

            label = pygame.font.Font(None, fontSize).render('Score:'+str(score[i][0]) , 1,fontColor)
            pos = (x+10,pos[1]+dy)
            surface.blit(label,pos)

            label = pygame.font.Font(None, fontSize).render('Date:'+str(score[i][1]) , 1,fontColor)
            pos = (x+145,pos[1])
            surface.blit(label,pos)
            pos = (x,pos[1]+dy)

# when users get hight score, remind to record
# new score board
class Alert(GameMenu):
    def __init__(self,pos,resource):
        self.pos = pos
        super(Alert,self).__init__(['Play Again','Back to Menu'],None,resource,(pos[0],pos[1]+180))
        self.keys =  map(chr,range(48,58)+range(65,91)+range(97,123))
        self.string = ''
        self.textBoxPos = (pos[0]-90,250)
        self.string =''
        self.cur_pos = 0
    #
    # draw to imput user name
    def draw(self,surface,score,textBox = False):
        super(Alert,self).draw(surface)

        if textBox:
            labelNewRecord = pygame.font.Font(None, 36).render('New Record!!!', 1,(255,255,0))
        else:
            labelNewRecord = pygame.font.Font(None, 36).render('Game Over', 1,(255,255,0))
        pos = (self.pos[0]-labelNewRecord.get_width()/2,self.pos[1])
        surface.blit(labelNewRecord,pos)

        labelScore = pygame.font.Font(None, 36).render('Score:'+str(score), 1,(255,255,0))
        pos = (self.pos[0]-labelScore.get_width()/2,self.pos[1]+50)
        surface.blit(labelScore,pos)

        if textBox:
            rect = pygame.Rect(self.textBoxPos,(180,50))
            pygame.draw.rect(surface,(0,0,0),rect,0)
            rect.move_ip(9,rect.height/4)
            text = pygame.font.Font(None, 36).render(self.string, 1, (255,255,255))
            surface.blit(text,rect.topleft)
            if (pygame.time.get_ticks()/500)%2 == 0 and len(self.string)<10:
                rect.width = 18
                rect.height = 25
                rect.move_ip(text.get_width(),0)
                pygame.draw.rect(surface,(255,255,255),rect,0)

    def control(self):

        for event in pygame.event.get():
            returnValue = super(Alert,self).subControl(event)
            if not returnValue is None:
                return returnValue

            if event.type == pygame.KEYDOWN and pygame.key.name(event.key) in self.keys:
                if len(self.string)<10:
                    self.string += pygame.key.name(event.key)
                    self.string = self.string.upper()
            if event.type == pygame.KEYDOWN and event.key == pygame.K_BACKSPACE:
                if len(self.string) > 0:
                    self.string = self.string[0:-1]



    def __str__(self):
        return self.string
