# Tetris-PyGame
This is reimplement of Tetris game in Python 2.7 using PyGame (Free Open Source Library).
The puprose of this project is to be using in coursework in university.

##### Run
If PyGame is already install we can simply run the main.py file
```
python main.py
```

##### About Play

1. Game rule
    - Settle the dropping down blocks with different shapes. Empty lines by filling them with blocks. Level will rise when lines are emptied, which leads to faster block dropping speed. Game over when blocks reach ceiling of the box. User can hold one dropping block and use it later.

    - Score calculation: On level n, n scores will be earned by using soft drop per time. Hard drop is considered as the corresponding number of soft drops.

    - Line calculation: On level n, n lines are needed to be emptied for moving to the next level.

    - Block dropping speed: On level 1, block dropping speed is 2 seconds per line. When moving into an upper level, the speed is 80% of the former one.

2. Play control
      - Control by keyboard
        - Left/Right button: move left/right
        - Down button: soft drop
        - Space button: hard drop
        - ‘Z’/ ‘X’ button: rotate left/right
        - ‘C’ button: hold
        - ‘P’ button: pause

3. Display
    - Red box in the left: hold a block; Level, Line, and Score are showed below the red box.
    - Blue box in the middle: main play area
    - Yellow box and blue box in the right: show the next four dropping blocks. The one in yellow box is the next dropping one.
