import sys
import pygame
WHITE = (255, 255, 255)
class MenuItem():
    def __init__(self, text, font=None, font_size=36,
                 font_color=WHITE, (pos_x, pos_y)=(0, 0)):
        self.text = text
        self.font = font
        self.font_size = font_size
        self.font_color = font_color
        self.font_italic = False
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.position = pos_x, pos_y
        self.fontobj = pygame.font.Font(self.font,self.font_size)
        self.label = self.fontobj.render(self.text, True, self.font_color)

        self.width = self.label.get_rect().width
        self.height = self.label.get_rect().height
        self.dimensions = (self.width, self.height)
 
    def set_position(self, x, y):
        self.position = (x, y)
        self.pos_x = x
        self.pos_y = y
 
    def set_font_color(self, rgb_tuple):
        self.font_color = rgb_tuple
        self.label = self.fontobj.render(self.text, 1, self.font_color)
    def set_italic(self, attribution):
        self.font_italic = attribution
        self.fontobj.set_italic(attribution)
    def get_height(self):
        return self.height
