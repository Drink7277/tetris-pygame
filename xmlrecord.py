import os, sys
import pygame
import time
import base64
import xml.etree.ElementTree as ET
from datetime import *
from const import *


class Xmlrecord(object):
    MAX = SCOREBOARD_SIZE()
    def __init__(self,filename='record.xml'):
        self.elemTree = ET.ElementTree()
        self.filename = filename

        if not self.have_file():
            self.creatXml()

        self.parsexml()

        if self.root[0].attrib['Value'] != 'True':
            self.creatXml()
            self.parsexml()

    def parsexml(self):
        self.tree = self.elemTree.parse(self.filename)
        self.root = self.elemTree.getroot()

    def have_file(self):
        if not os.path.isfile(self.filename):
            return False
        return True
    
    def getscore(self, num=5):
        scorelist = []
        if num > Xmlrecord.MAX:
            num = Xmlrecord.MAX
        i = 0
        for score in self.root[0] :
            if int(score[1].text) >=0 and i < num:
                scorelist +=[[int(score[1].text),score[2].text,score[0].text]]
            i +=1
        if len(scorelist) == 0 :
            scorelist = [[0,'','']]
        else :
            scorelist.sort(reverse=True)
        return scorelist

    def update_record(self,name='', score=0):
        scorelist = []
        scorelist = self.getscore()

        strtoday = str(date.today())
        scorelist[Xmlrecord.MAX-1][0] = score
        scorelist[Xmlrecord.MAX-1][1] = strtoday
        scorelist[Xmlrecord.MAX-1][2] = name

        if len(scorelist)>1:
            scorelist.sort(reverse=True)
        for i,user in enumerate(self.root[0]) :
            if i < len(scorelist):
                user[0].text = str(scorelist[i][2])
                user[1].text = str(scorelist[i][0])
                user[2].text = str(scorelist[i][1])

            if i >= Xmlrecord.MAX-1 :
                break
            i +=1
        self.elemTree.write(self.filename,'utf-8',True)

    def creatXml(self):
        userinfo = ET.Element('UserDefaultRoot')
        self.elemTree._setroot(userinfo)
        
        ishavefile = ET.Element('isHaveSaveFileXml',{'Value':'True'})
        userinfo.append(ishavefile)
              
        for i in xrange(Xmlrecord.MAX) :
            user = ET.Element('user')
            ishavefile.append(user)

            ET.SubElement(user,'name').text = 'NONAME'
            ET.SubElement(user,'score').text = '0'
            ET.SubElement(user,'date').text = str(date.today())

        self.indent(userinfo) 
        self.elemTree.write(self.filename,'utf-8',True)

    # Get pretty look
    def indent(self,elem, level=0):
        i = "\n" + level * " "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            for e in elem:
                self.indent(e, level+1)
            if not e.tail or not e.tail.strip():
                e.tail = i
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
        return elem
