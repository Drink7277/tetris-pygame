import pygame

def SCREEN_SIZE():
    return (800,600)
    #pixels x pixels

def FRAME_RATE():
    return 30
    #frames per second

def BLOCK_SIZE():
    return 20
    #pixels

def NEXTBOX_CAPACITY():
    return 4
    #shapes
def NEXTBOX_SPACE():
    return 30

def HOLDBOX_SIZE():
    return 4
    #blocks x blocks

def PLAYBOX_SIZE():
    return (10,21)
    #blocks x blocks

def FPS_SHOW():
    return False
    # using for debuging

def HIDDENBOX_SIZE():
    return 4
    # blocks x blocks

def LABEL_COLOR():
    return (255,255,255)
    # score color in game play
    
def LABEL_FONT_SIZE():
    return 30
    # label on game play

def LINE_DONE():
    return 10
    # points are given when get the full line. (10 x level)

def BOX_COLOR():
    return (76,62,62)
    # background color of each box

def TIME_LIMIT():
    return 2.0
    # The maximum time before auto drop

def TIME_DECREASE():
    return 0.8
    # when level up the time will be 80% of the pevious level

def SCOREBOARD_SIZE():
    return 5
    # how many top scores are stored


#----------Game control----------#
def DROP_KEY():
    return pygame.K_DOWN

def HOLD_KEY():
    return pygame.K_c

def CCW_KEY():
    return pygame.K_z

def CW_KEY():
    return pygame.K_x

def HARD_DROP_KEY():
    return pygame.K_SPACE

def MOVE_LEFT_KEY():
    return pygame.K_LEFT

def MOVER_RIGHT_KEY():
    return pygame.K_RIGHT

def PAUSE_KEY():
    return pygame.K_p
