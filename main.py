import pygame
import math
import random
import time
import os
from xmlrecord import Xmlrecord
from const import *
from tetrisClass import *
from gamemenu import GameMenu, Alert, Score

class State(object):

    def __init__(self,stateList):
        if len(stateList) == 0:
            raise ValueError, 'stateList must contain list of string'
        self.valueList = stateList
        self.value = self.valueList[0]

    def changeState(self,state):
        if state is None:
            return
        if not state in self.valueList:
            raise ValueError, 'change state error'
        self.value = state

class Resource(object):
    picDirectory = 'pic'
    soundDirectory = 'sound'

    def __init__(self):
        self.picDic = {}
        self.soundDic = {}
        #using for score
        self.xml = Xmlrecord()
        #pictures
        self.insertPic('bgImage.bmp')
        self.insertPic('r1player.bmp')
        self.insertPic('score.bmp')
        #sounds
        self.insertSound('bgSound.ogg')
        self.insertSound('move.ogg')
        self.insertSound('empty.ogg')
        self.insertSound('place.ogg')
        self.insertSound('levelUp.ogg')
        self.insertSound('pause.ogg')
        self.insertSound('gameOver.ogg')



    def insertSound(self,str):
        name,nametype = str.split('.')

        filename = Resource.soundDirectory+os.sep+str

        if os.path.exists(filename):
            self.soundDic[name] = pygame.mixer.Sound(filename)
        else:
            raise IOError, 'File %s not found'%filename
    def insertPic(self,str):
        name,nametype = str.split('.')
        filename = Resource.picDirectory+os.sep+str
        if os.path.exists(filename):
            self.picDic[name] = pygame.image.load(filename)
        else:
            raise IOError, 'File %s not found'%filename

    def playSound(self,str,n=0):
        self.soundDic[str].play(n)

    def getPic(self,str):
        return self.picDic[str]

    def getHighScore(self):
        return self.xml.getscore()

    def updateScore(self,name,score):
        self.xml.update_record(name,score)

def main():

    #----------Initialize----------#
    pygame.init()
    state =  State(['Menu','Single Player','Score', 'Quit'])
    resource = Resource()
    allBox   = AllBox((0,0),(255,255,255),resource)
    gameMenu = GameMenu(state.valueList[1:],resource.getPic('bgImage'),resource);
    score    = Score(resource.getPic('score'),(250,160),resource)
    pygame.display.set_caption("TETRIS")
    screen = pygame.display.set_mode(SCREEN_SIZE())
    resource.playSound('bgSound',-1)
    clock = pygame.time.Clock()
    #-----use for debuging-----#
    if FPS_SHOW():
        myfont = pygame.font.Font(None, 20)
        mytime = [time.time(),time.time()]
    #-------------------------#
    while True:
        clock.tick(FRAME_RATE())

        if state.value == 'Menu':
            state.changeState(gameMenu.control())
            gameMenu.draw(screen)
        elif state.value == 'Single Player':
            state.changeState(allBox.update())
            allBox.draw(screen)
        elif state.value ==  'Score':
            state.changeState(score.control())
            score.draw(screen)
        elif state.value ==  'Quit':
            exit()
        #-----use for debuging-----#
        if FPS_SHOW():
            mytime = [mytime[1],time.time()]
            try:
                temp = 1/(mytime[1]-mytime[0])
            except:
                temp = 1
            label = myfont.render("FPS = %.2f hz"%temp, 1, (255,255,0))
            screen.blit(label,(700,10))
        #--------------------------#
        pygame.display.flip()
#----------main----------
if __name__ == '__main__': main()

