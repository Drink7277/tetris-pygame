import pygame
import sys
import time
import copy
import random
from gamemenu import GameMenu
from const import *
from main import State
from gamemenu import Alert


# This function draws the single block by using position in put pos
def drawSingleBlock( surface, pos, color):
        # n = value of line before fade in to color value
        n = 5
        for i in range(1,n):
            #draw the brighter part
            pygame.draw.lines(surface, (255-(255-color[0])*i/n,255-(255-color[1])*i/n,255-(255-color[2])*i/n), False,
                             [(pos[0]+i,pos[1]+i),
                              (pos[0]-i+BLOCK_SIZE()-1,pos[1]+i),
                              (pos[0]-i+BLOCK_SIZE()-1,pos[1]-i+BLOCK_SIZE()-1)
                             ],1)
            #draw the darker part
            pygame.draw.lines(surface,(color[0]*i/n,color[1]*i/n,color[2]*i/n), False,
                             [(pos[0]+i,pos[1]+i),
                              (pos[0]+i,pos[1]-i+BLOCK_SIZE()-1),
                              (pos[0]-i+BLOCK_SIZE()-1,pos[1]-i+BLOCK_SIZE()-1)
                             ],1)
        #draw the middle square with color
        pygame.draw.rect(surface, color, pygame.Rect((pos[0]+n,pos[1]+n),(BLOCK_SIZE()-2*n,BLOCK_SIZE()-2*n)), 0)

# This function will return the TetrisShape object with the random shape value
def randTetrishape():
    randomShape = TetrisShape.valueList[random.randint(1,len(TetrisShape.valueList)-1)]
    return TetrisShape(randomShape)

# This class store the information of all of the shapes and their colors
class TetrisShape(object):
    #value can be only anything in valueList
    #colorDic stores the color for each value
    valueList = [' ','o', 'i', 't', 's', 'z', 'l', 'j']
    colorDic = {' ':(255,255,255),'o':(255,255,0),'i':(0,255,255),'t':(255,0,255),'s':(0,255,0),'z':(255,0,0),'l':(255,128,0),'j':(0,0,255)}

    def __init__(self,value):
        if( not value in TetrisShape.valueList):
            raise ValueError, 'value need to be in the TetrisShape.valueList'

        self.value = value
        self.color = TetrisShape.colorDic[value]

    # return the rect for drawing the shape
    def getRect(self,offset=(0,0)):
        if self.value == ' ':
            return pygame.Rect(offset , (1,1))
        if self.value == 'o':
            return pygame.Rect(offset , (BLOCK_SIZE()*2,BLOCK_SIZE()*2))
        if self.value == 'i':
            return pygame.Rect(offset , (BLOCK_SIZE()*4,BLOCK_SIZE()*1))
        if self.value == 't':
            return pygame.Rect(offset , (BLOCK_SIZE()*3,BLOCK_SIZE()*2))
        if self.value == 's':
            return pygame.Rect(offset , (BLOCK_SIZE()*3,BLOCK_SIZE()*2))
        if self.value == 'z':
            return pygame.Rect(offset , (BLOCK_SIZE()*3,BLOCK_SIZE()*2))
        if self.value == 'l':
            return pygame.Rect(offset , (BLOCK_SIZE()*2,BLOCK_SIZE()*3))
        if self.value == 'j':
            return pygame.Rect(offset , (BLOCK_SIZE()*2,BLOCK_SIZE()*3))

    # draw by using center point
    def draw(self, surface, center):
        rect = self.getRect(center)
        rect = rect.move(-rect.width/2,-rect.height/2)

        if self.value == ' ':
            return
        if self.value == 'o':
            drawSingleBlock(surface, rect.topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),0).topleft,self.color)
            drawSingleBlock(surface, rect.move(0,BLOCK_SIZE()).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),BLOCK_SIZE()).topleft,self.color)
            return
        if self.value == 'i':
            drawSingleBlock(surface, rect.topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),0).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE()*2,0).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE()*3,0).topleft,self.color)
            return
        if self.value == 't':
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),0).topleft,self.color)
            drawSingleBlock(surface, rect.move(0,BLOCK_SIZE()).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),BLOCK_SIZE()).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE()*2,BLOCK_SIZE()).topleft,self.color)
            return
        if self.value == 's':
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),0).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE()*2,0).topleft,self.color)
            drawSingleBlock(surface, rect.move(0,BLOCK_SIZE()).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),BLOCK_SIZE()).topleft,self.color)
            return
        if self.value == 'z':
            drawSingleBlock(surface, rect.move(0,0).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),0).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),BLOCK_SIZE()).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE()*2,BLOCK_SIZE()).topleft,self.color)
            return
        if self.value == 'l':
            drawSingleBlock(surface, rect.move(0,0).topleft,self.color)
            drawSingleBlock(surface, rect.move(0,BLOCK_SIZE()).topleft,self.color)
            drawSingleBlock(surface, rect.move(0,BLOCK_SIZE()*2).topleft,self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),BLOCK_SIZE()*2).topleft,self.color)
            return
        if self.value == 'j':
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),0).topleft, self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),BLOCK_SIZE()).topleft, self.color)
            drawSingleBlock(surface, rect.move(0,BLOCK_SIZE()*2).topleft, self.color)
            drawSingleBlock(surface, rect.move(BLOCK_SIZE(),BLOCK_SIZE()*2).topleft, self.color)
            return

class Box(object):
    #rect is pygame.Rect object
    #label is string can be None
    def __init__(self,rect,color,label):

        self.rect  = rect
        self.color = color
        self.label = label

    def draw(self,surface):
        pygame.draw.rect(surface, self.color, self.rect, 0)
        # draw the label of the box
        if (self.label is None):
            return
        labelSur = pygame.font.Font(None, LABEL_FONT_SIZE()).render(self.label, 1, LABEL_COLOR())
        pos = (self.rect.left+(self.rect.width-labelSur.get_width())/2,self.rect.top-labelSur.get_height())
        surface.blit(labelSur,pos)

class HoldBox(Box):

    def __init__(self, pos, color, shape = TetrisShape(' ')):
        super(HoldBox,self).__init__(pygame.Rect(pos,(BLOCK_SIZE()*HOLDBOX_SIZE(),BLOCK_SIZE()*HOLDBOX_SIZE())),color,None)
        self.shape = shape

    def draw(self,surface):
        #super(HoldBox, self).draw(surface)
        self.shape.draw(surface,self.rect.center)

    def getHold(self,shape):
        out = self.shape
        self.shape = shape
        return out

class NextBox(Box):

    def __init__(self, pos, color):
        super(NextBox,self).__init__(pygame.Rect(pos,(BLOCK_SIZE()*HOLDBOX_SIZE(),BLOCK_SIZE()*HOLDBOX_SIZE()*NEXTBOX_CAPACITY())),color,None)
        self.shapeList = []
        for i in xrange(NEXTBOX_CAPACITY()):
            self.shapeList.append(randTetrishape())

    def draw(self,surface):
        #super(NextBox, self).draw(surface)
        self.shapeList[0].draw(surface,(self.rect.centerx,self.rect.top+BLOCK_SIZE()*2))
        for i in xrange(1,len(self.shapeList)):
            self.shapeList[i].draw(surface,(self.rect.centerx,self.rect.top+BLOCK_SIZE()*(2+4*i)+NEXTBOX_SPACE()))

    def getNext(self):
        out = self.shapeList[0]
        for i in xrange(NEXTBOX_CAPACITY()-1):
            self.shapeList[i] = self.shapeList[i+1]
        self.shapeList[NEXTBOX_CAPACITY()-1] = randTetrishape()
        return out

class HiddenBox(Box):
    def __init__(self,playBox,shape):
        super(HiddenBox,self).__init__(pygame.Rect((0,0),(HIDDENBOX_SIZE()*BLOCK_SIZE(),HIDDENBOX_SIZE()*BLOCK_SIZE())),(100,100,100),None)
        self.playBox = playBox
        self.empty()
        self.setShape(shape)

    def empty(self):
        self.store = [[' ' for i in xrange(HIDDENBOX_SIZE())] for j in xrange(HIDDENBOX_SIZE())]

    def draw(self, surface):
        #super(HiddenBox,self).draw(surface)
        for i in xrange(len(self.store)):
            for j in xrange(len(self.store[i])):
                if self.store[i][j] != ' ':
                    col  = BLOCK_SIZE()*j
                    row  = BLOCK_SIZE()*i
                    pos = self.rect.move(col,row).topleft
                    if pos[1] >= self.playBox.rect.top-BLOCK_SIZE():
                        drawSingleBlock(surface,pos,self.shape.color)

    def move(self,x,y):
        if self.isValid(x,y,self.store):
            self.rect = self.rect.move(x*BLOCK_SIZE(),y*BLOCK_SIZE())

    # generate and check before assigning
    def rotateCW(self):
        temp = [[' ' for i in xrange(HIDDENBOX_SIZE())] for j in xrange(HIDDENBOX_SIZE())]
        for i in xrange(HIDDENBOX_SIZE()):
            for j in xrange(HIDDENBOX_SIZE()):
                temp[i][j] = self.store[HIDDENBOX_SIZE()-j-1][i]
        if self.isValid(0,0,temp):
            self.store = temp

    def rotateCCW(self):
        temp = [[' ' for i in xrange(HIDDENBOX_SIZE())] for j in xrange(HIDDENBOX_SIZE())]
        for i in xrange(HIDDENBOX_SIZE()):
            for j in xrange(HIDDENBOX_SIZE()):
                temp[i][j] = self.store[j][HIDDENBOX_SIZE()-i-1]
        if self.isValid(0,0,temp):
            self.store = temp

    def isValid(self,x,y,store):
        temp = self.rect.move(x*BLOCK_SIZE(),y*BLOCK_SIZE())
        for i in xrange(len(store)):
            for j in xrange(len(store[i])):
                if store[i][j] != ' ':
                    px = (temp.left+j*BLOCK_SIZE()-self.playBox.rect.move(0,-HIDDENBOX_SIZE()*BLOCK_SIZE()).left)/BLOCK_SIZE()
                    py = (temp.top +i*BLOCK_SIZE()-self.playBox.rect.move(0,-HIDDENBOX_SIZE()*BLOCK_SIZE()).top)/BLOCK_SIZE()
                    if px >= 0 and px < PLAYBOX_SIZE()[0]\
                        and py >= 0 and py < PLAYBOX_SIZE()[1]+HIDDENBOX_SIZE()\
                        and self.playBox.store[py][px] == ' ':
                        continue
                    else:
                        return False
        return True

    def setShape(self,shape):
        self.empty()
        self.shape = shape
        if shape.value == 'o':
            self.store[1][1] = shape.color
            self.store[1][2] = shape.color
            self.store[2][1] = shape.color
            self.store[2][2] = shape.color
        if shape.value == 'i':
            self.store[2][0] = shape.color
            self.store[2][1] = shape.color
            self.store[2][2] = shape.color
            self.store[2][3] = shape.color
        if shape.value == 't':
            self.store[2][1] = shape.color
            self.store[2][2] = shape.color
            self.store[2][3] = shape.color
            self.store[1][2] = shape.color
        if shape.value == 's':
            self.store[1][3] = shape.color
            self.store[1][2] = shape.color
            self.store[2][1] = shape.color
            self.store[2][2] = shape.color
        if shape.value == 'z':
            self.store[1][0] = shape.color
            self.store[1][1] = shape.color
            self.store[2][1] = shape.color
            self.store[2][2] = shape.color
        if shape.value == 'l':
            self.store[0][1] = shape.color
            self.store[1][1] = shape.color
            self.store[2][1] = shape.color
            self.store[2][2] = shape.color
        if shape.value == 'j':
            self.store[0][2] = shape.color
            self.store[1][2] = shape.color
            self.store[2][2] = shape.color
            self.store[2][1] = shape.color

class PlayBox(Box):

    def __init__(self, pos, color, shape):
        super(PlayBox,self).__init__(pygame.Rect(pos,(BLOCK_SIZE()*PLAYBOX_SIZE()[0],BLOCK_SIZE()*PLAYBOX_SIZE()[1])),color,None)
        self.resetStore()
        self.hiddenBox = HiddenBox(self,shape)
        self.hiddenBoxResetPosition()

    def resetStore(self): #empty
        self.store = [[' ' for i in xrange(PLAYBOX_SIZE()[0])] for j in xrange(PLAYBOX_SIZE()[1]+HIDDENBOX_SIZE())]

    def draw(self,surface,color = None):
        super(PlayBox,self).draw(surface)
        for i in xrange(HIDDENBOX_SIZE(),len(self.store)):
            for j in xrange(len(self.store[i])):
                if self.store[i][j] != ' ':
                    if color is None:
                        drawSingleBlock(surface, self.rect.move(BLOCK_SIZE()*j,BLOCK_SIZE()*(i-HIDDENBOX_SIZE())).topleft,self.store[i][j])
                    else:
                        drawSingleBlock(surface, self.rect.move(BLOCK_SIZE()*j,BLOCK_SIZE()*(i-HIDDENBOX_SIZE())).topleft,color)
        self.hiddenBox.draw(surface)

    def moveHiddenBox(self,x,y):
        self.hiddenBox.move(x , y)

    def hiddenBoxResetPosition(self):
        self.hiddenBox.rect.topleft = (self.rect.left+int((PLAYBOX_SIZE()[0]-HIDDENBOX_SIZE())/2)*BLOCK_SIZE(),self.rect.top-(HIDDENBOX_SIZE()*BLOCK_SIZE()))

    def hiddenBoxSetShape(self,shape):
        self.hiddenBox.setShape(shape)

    def hiddenBoxMap(self,shape):
        for i in xrange(HIDDENBOX_SIZE()):
            for j in xrange(HIDDENBOX_SIZE()):
                if self.hiddenBox.store[i][j] != ' ':
                    px = (self.hiddenBox.rect.left+j*BLOCK_SIZE()-self.rect.move(0,-HIDDENBOX_SIZE()*BLOCK_SIZE()).left)/BLOCK_SIZE()
                    py = (self.hiddenBox.rect.top +i*BLOCK_SIZE()-self.rect.move(0,-HIDDENBOX_SIZE()*BLOCK_SIZE()).top)/BLOCK_SIZE()
                    self.store[py][px] = self.hiddenBox.store[i][j]
        self.hiddenBox.setShape(shape)
        self.hiddenBoxResetPosition()

    def getFullLine(self):
        line  = []
        for i in xrange(HIDDENBOX_SIZE(),len(self.store)):
            for j in xrange(len(self.store[i])):
                if self.store[i][j] == ' ':
                    break
                if j == len(self.store[i])-1:
                    line.append(i)

        return line

    def removeFullLine(self,line):
        if len(line) != 0:
            for i in line:
                for j in xrange(i,1,-1):
                    for k in xrange(PLAYBOX_SIZE()[0]):
                        self.store[j][k] = self.store[j-1][k]

    def isGameOver(self):

        for i in xrange(PLAYBOX_SIZE()[0]):
            if self.store[HIDDENBOX_SIZE()-1][i] != ' ':
                return True
        return False


# put every box together
class AllBox(Box):

    def __init__(self,pos,color,resource):
        super(AllBox,self).__init__(
            pygame.Rect(pos,(BLOCK_SIZE()*(4+2*HOLDBOX_SIZE()+PLAYBOX_SIZE()[0]),BLOCK_SIZE()*(2+PLAYBOX_SIZE()[1])))
            ,color,None)
        self.resource   = resource
        self.holdBox    = HoldBox((180,120),BOX_COLOR())
        self.nextBox    = NextBox((540,120),BOX_COLOR())
        self.playBox    = PlayBox((300,100),BOX_COLOR(),self.nextBox.getNext())
        self.alert      = Alert((SCREEN_SIZE()[0]/2,SCREEN_SIZE()[1]/4),resource)
        self.background = self.resource.getPic('r1player')
        self.pauseMenu  = GameMenu(('Resume','Restart','Back to Menu'),None,resource,(SCREEN_SIZE()[0]/2,SCREEN_SIZE()[1]/2))
        self.state      = State(['restart','play','pause','animation','gameOver'])

        #using to draw the animation [fameLeft,list of line]
        self.animationTemp = [0,[]]

        self.restart()



    def restart(self):
        #Maximum period before forcing drop
        self.timeLimit = TIME_LIMIT()
        #Time left before drop
        self.timeLeft = self.timeLimit
        #using calculate time between pevious frame and current frame
        self.time = [time.time(),time.time()];
        #hold action
        self.holdEnable = True
        #line left for level up
        self.line     = 1
        self.level    = 1
        self.score    = 0

        self.playBox.hiddenBox.setShape(self.nextBox.getNext())
        self.playBox.hiddenBoxResetPosition()
        self.playBox.resetStore()
        self.state.changeState('play')

    #----------control game----------
    def rotateCCW(self):
        self.playBox.hiddenBox.rotateCCW()
    def rotateCW(self):
        self.playBox.hiddenBox.rotateCW()
    #using during developing process only
    def moveUp(self):
        self.playBox.moveHiddenBox(0,-1)
    def moveLeft(self):
        self.playBox.moveHiddenBox(-1,0)
    def moveDown(self):
        if self.playBox.hiddenBox.isValid(0,1,self.playBox.hiddenBox.store):
            self.playBox.moveHiddenBox(0,1)
        else:
            self.map()
    def hardDrop(self):
        while self.playBox.hiddenBox.isValid(0,1,self.playBox.hiddenBox.store):
            self.playBox.moveHiddenBox(0,1)
            self.score += self.level
        self.map()
    def moveRight(self):
        self.playBox.moveHiddenBox(1,0)
    def map(self):
        self.resource.playSound('place')
        self.playBox.hiddenBoxMap(self.nextBox.getNext())
        line = self.playBox.getFullLine()
        if len(line) != 0:
            self.resource.playSound('empty')
            self.state.changeState('animation')
            self.animationTemp = [10,line]
        self.line -= len(line)
        self.score += len(line)*LINE_DONE()*self.level

        if self.playBox.isGameOver():    
            self.resource.playSound('gameOver')
            self.state.changeState('gameOver')
        self.holdEnable = True
        while self.line <= 0:
            self.resource.playSound('levelUp')
            self.level += 1
            self.line +=  self.level

    def hold(self):
        if not self.holdEnable:
                return
        
        temp = self.holdBox.getHold(self.playBox.hiddenBox.shape)
        if temp.value == ' ':
            self.playBox.hiddenBoxSetShape(self.nextBox.getNext())
        else:
            self.playBox.hiddenBoxSetShape(temp)
        self.playBox.hiddenBoxResetPosition()
        self.holdEnable = False

    #--------------------

    def isTopScore(self):
        list = self.resource.getHighScore()
        if self.score > list[SCOREBOARD_SIZE()-1][0]:
            return True
        else:
            return False


    def control(self):
        if self.state.value == 'play':
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit()
                elif event.type == pygame.KEYDOWN:

           
                    if event.key == HOLD_KEY():
                        self.hold()
                    elif event.key == CW_KEY():
                        self.resource.playSound('move')
                        self.rotateCW()
                    elif event.key == CCW_KEY():
                        self.resource.playSound('move')
                        self.rotateCCW()
                    elif event.key == HARD_DROP_KEY():
                        self.hardDrop()
                    elif event.key == MOVE_LEFT_KEY():
                        self.resource.playSound('move')
                        self.moveLeft()
                    elif event.key == MOVER_RIGHT_KEY():
                        self.resource.playSound('move')
                        self.moveRight()
                    elif event.key == DROP_KEY():
                        self.resource.playSound('move')
                        self.score += self.level
                        self.moveDown()
                    elif event.key == PAUSE_KEY():
                        self.state.changeState('pause')

        elif self.state.value == 'pause':
            for event in pygame.event.get():
                returnValue = self.pauseMenu.subControl(event)
                if event.type == pygame.KEYDOWN and event.key == PAUSE_KEY():
                     return 'Resume'
                if not returnValue is None:
                    return returnValue
        elif self.state.value == 'restart':
            self.restart()
        elif self.state.value == 'gameOver':
            return self.alert.control()

    def draw(self,surface):

        if self.state.value == 'play':
            #super(AllBox,self).draw(surface)
            surface.blit(self.background, (0, 0))
            self.holdBox.draw(surface)
            self.nextBox.draw(surface)
            self.playBox.draw(surface)

            labelLevel = pygame.font.Font(None, LABEL_FONT_SIZE()).render( str(self.level), 1, LABEL_COLOR())
            pos = (220-labelLevel.get_width()/2,300)
            surface.blit(labelLevel,pos)
            labelLine = pygame.font.Font(None, LABEL_FONT_SIZE()).render(str(self.line), 1, LABEL_COLOR())
            pos = (220-labelLevel.get_width()/2,380)
            surface.blit(labelLine,pos)
            labelScore = pygame.font.Font(None, LABEL_FONT_SIZE()).render(str(self.score), 1, LABEL_COLOR())
            pos = (220-labelScore.get_width()/2,460)
            surface.blit(labelScore,pos)

        elif self.state.value == 'pause':
            self.playBox.draw(surface,BOX_COLOR())
            self.pauseMenu.draw(surface)

            label = pygame.font.Font(None,60).render('Pause', True, (255,255,0))
            pos = ((SCREEN_SIZE()[0]-label.get_width())/2,150)
            surface.blit(label,pos)

        elif self.state.value == 'animation':

            self.playBox.draw(surface)

            for i in self.animationTemp[1]:
                for j in xrange(len(self.playBox.store[i])):

                    r,g,b    = self.playBox.store[i][j]
                    rr,gg,bb = BOX_COLOR()
                    dr,dg,db = r-rr,g-gg,b-bb

                    color    = (rr+(dr*self.animationTemp[0])/10,gg+(dg*self.animationTemp[0])/10,bb+(db*self.animationTemp[0])/10)
                    drawSingleBlock(surface, self.playBox.rect.move(BLOCK_SIZE()*j,BLOCK_SIZE()*(i-HIDDENBOX_SIZE())),color)

            self.animationTemp[0] -=1
            if self.animationTemp[0] <= 0:
                self.state.changeState( 'play')
                self.playBox.removeFullLine(self.animationTemp[1])

        elif self.state.value == 'gameOver':
            surface.blit(self.background, (0, 0))
            self.playBox.draw(surface,BOX_COLOR())
            labelLevel = pygame.font.Font(None, LABEL_FONT_SIZE()).render( str(self.level), 1, LABEL_COLOR())
            pos = (220-labelLevel.get_width()/2,300)
            surface.blit(labelLevel,pos)
            labelLine = pygame.font.Font(None, LABEL_FONT_SIZE()).render(str(self.line), 1, LABEL_COLOR())
            pos = (220-labelLevel.get_width()/2,380)
            surface.blit(labelLine,pos)
            labelScore = pygame.font.Font(None, LABEL_FONT_SIZE()).render(str(self.score), 1, LABEL_COLOR())
            pos = (220-labelScore.get_width()/2,460)
            surface.blit(labelScore,pos)
            self.alert.draw(surface,self.score,self.isTopScore())

    def update(self):

        control = self.control()
        if self.state.value == 'play':
            self.time = [self.time[1],time.time()]
            self.timeLeft -= (self.time[1]-self.time[0])
            if self.timeLeft <= 0:
                self.moveDown()
                self.timeLeft = self.timeLimit * (TIME_DECREASE()**(self.level-1))
        elif self.state.value == 'pause':

            if control is None:
                return
            elif control == 'Resume':
                self.state.changeState('play')
            elif control == 'Back to Menu':
                self.state.changeState('restart')
                return 'Menu'
            elif control == 'Restart':
                self.state.changeState('restart')

        elif self.state.value == 'animation':
            pass

        elif self.state.value == 'gameOver':
            if control is None:
                return
            elif self.isTopScore():
                self.resource.updateScore(self.alert.string,self.score)

            if control == 'Back to Menu':
                self.state.changeState('restart')
                return 'Menu'
            elif control == 'Play Again':
                self.state.changeState('restart')


             
